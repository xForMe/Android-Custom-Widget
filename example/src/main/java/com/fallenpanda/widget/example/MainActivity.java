package com.fallenpanda.widget.example;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.fallenpanda.widget.library.calendarview.CalendarView;
import com.fallenpanda.widget.library.calendarview.Util;
import com.fallenpanda.widget.library.calendarview.onCalendarViewListener;

public class MainActivity extends Activity {

	CalendarView mf;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mf = (CalendarView) findViewById(R.id.mFCalendarView);

		mf.setOnCalendarViewListener(new onCalendarViewListener() {
			
			@Override
			public void onDisplayedMonthChanged(int month, int year, String monthStr) {

				StringBuffer bf = new StringBuffer()
				.append(" month:")
				.append(month)
				.append(" year:")
				.append(year)
				.append(" monthStr: ")
				.append(monthStr);
				
				Toast.makeText(MainActivity.this,  bf.toString(),
						Toast.LENGTH_SHORT).show();
				
			}
			
			@Override
			public void onDateChanged(String date) {
			
				Toast.makeText(MainActivity.this, "onDateChanged:" + date, 
						Toast.LENGTH_SHORT).show();
			}

            @Override
            public void onDateLongClick(String date) {

            }


        });
		
		/**
		 * you can set calendar date anytime
		 * */
		mf.setDate("2014-11-11");

		/**
		 * calendar events samples 
		 * */
		ArrayList<String> eventDays = new ArrayList<String>();
		eventDays.add("2014-11-11");
		eventDays.add(Util.getTomorrow());
		eventDays.add(Util.getCurrentDate());

		mf.setEvents(eventDays);

		Log.e("","locale:" + Util.getLocale());
		
	}
}
