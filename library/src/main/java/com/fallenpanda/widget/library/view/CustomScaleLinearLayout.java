package com.fallenpanda.widget.library.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.fallenpanda.widget.library.R;

/**
 * 自定义比例 LinearLayout（高度：宽度）
 *
 */

public class CustomScaleLinearLayout extends LinearLayout {

    private float custom_scale;

	public CustomScaleLinearLayout(Context context) {
        this(context, null);
	}

	public CustomScaleLinearLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
        TypedArray localTypedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomScaleLinearLayout);
        custom_scale = localTypedArray.getFloat(R.styleable.CustomScaleLinearLayout_custom_scale, 1.0f);
        localTypedArray.recycle();
	}
	
	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = Math.round(width*custom_scale);
        int mode = MeasureSpec.getMode(widthMeasureSpec);
        super.onMeasure(MeasureSpec.makeMeasureSpec(width, mode), MeasureSpec.makeMeasureSpec(height, mode));
    }
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
	    super.onSizeChanged(w, Math.round(w*custom_scale), oldw, oldh);
	}

}
