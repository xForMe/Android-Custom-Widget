package com.fallenpanda.widget.library.calendarview;

/**
 * 日历事件接口
 *
 */
public interface onCalendarViewListener {

	void onDateChanged(String date);
	
	void onDateLongClick(String date);

	/**
	 * @param month 月份 1-12
	 * @param year 年份
	 * @param monthStr 月份名称
	 */
	void onDisplayedMonthChanged(int month, int year, String monthStr);
	
}
