package com.fallenpanda.widget.library.calendarview;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@SuppressLint("DefaultLocale")
public class Util {

	/**
	 * 时间转换为毫秒
	 * 
	 * @param date "2013-12-17"
	 * */
	public static long dateToLong(String date){

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", getLocale());

		Date d = null;
		try {
			d = formatter.parse(date);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Calendar c = Calendar.getInstance();
		c.setTime(d);

		return c.getTimeInMillis();
	}

	/**
     * 得到当前日期
     *
	 * @return String
	 **/
	public static String getCurrentDate(){
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", getLocale());
		return df.format(c.getTime());
	}

    /**
     * 得到明天日期
     *
     * @return String
     **/
    public static String getTomorrow(){
        long oneDay = 86400000; // 86400000 miliseconds equals 1 day
        long tomorrow = dateToLong(getCurrentDate()) + oneDay;

        Calendar c = Calendar.getInstance();
        c.setTime(new Date(tomorrow));
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", getLocale());
        return df.format(c.getTime());
    }

	/**
     * 得到当前日期
     *
	 * @param format "yyyy-MM-dd"
     * @return String
	 **/
	public static String getCurrentDate(String format){
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat(format, getLocale());
		return df.format(c.getTime());
	}

	public static Locale getLocale(){
		return Locale.getDefault();
	}

}


